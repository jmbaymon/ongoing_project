import os
import subprocess as cmd
"""
The GitPy class contains methods that allows the user to
push, pull, clone, add and commit to repo. Repo  will ask at menu
w
"""
class GitPy:
    def __init__ (self, repo):
        self.repo = repo

        print("Repository set is " + self.repo)
    def dir_repo(self):
        path = os.getcwd()
        print(path)

        os



    def clone_git(self):
        git_clone = ['git', 'clone', self.repo] # keep arguments separate

        return cmd.call(git_clone)
    def push_repo(self):
        git_push = ['git', 'push', 'master'] # keep arguments separate
        return cmd.call(git_push)
    def pull_repo(self):
        git_pull = ['git', 'pull'] # keep arguments separate
        return cmd.call(git_pull)
    def add(self):
        git_add = ['git', 'add', '.'] # keep arguments separate
        return cmd.call(git_add)
    def commit(self):
        message = input("Enter comment:")
        git_commit = ['git', 'commit -m', message ] # keep arguments separate
        return cmd.call(git_commit)
