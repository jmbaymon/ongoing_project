from pygit import GitPy

def main():
    # User enters repository link from Github or other git source
    repo = input("Enter the link: ")
    ci_1 = GitPy(repo)

    while True:
        print("Add")
        print("Commit")
        print("Push")
        print("Pull")
        print("Clone")

        entry = input("Enter Selection: ")

        if entry == "Add":
            ci_1.add()
        elif entry == "Commit":
            ci_1.commit()
        elif entry == "Push":
            ci_1.push_repo()
        elif entry == "Pull":
            ci_1.pull_repo()
        elif entry == "Clone":
            ci_1.clone_git()





if __name__ == '__main__':
    main()
